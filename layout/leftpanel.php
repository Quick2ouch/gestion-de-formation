  <div class="leftpanel">
    
    <div class="logopanel">
        <h1><span>[</span> Go Formation <span>]</span></h1>
    </div><!-- logopanel -->
    
    <div class="leftpanelinner">
        
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
                <img alt="" src="images/photos/loggeduser.png" class="media-object">
                <div class="media-body">
                    <h4>John Doe</h4>
                    <span>"Life is so..."</span>
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Mon Compte</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>
              <li><a href="#"><i class="fa fa-cog"></i> <span>Paramétre</span></a></li>
              <li><a href="#"><i class="fa fa-question-circle"></i> <span>Aide</span></a></li>
              <li><a href="signout.html"><i class="fa fa-sign-out"></i> <span>Déconnexion</span></a></li>
            </ul>
        </div>
      
      <h5 class="sidebartitle">Navigation</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li><a href="index.php" id="menu_dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <li class="nav-parent"><a href="#"><i class="fa fa-graduation-cap"></i> <span>Étudiants</span></a>
          <ul class="children">
            <li id="menu_etudiant_etudiant"><a href="etudiant.php"><i class="fa fa-caret-right"></i> Etudiants</a></li>
            <li id="menu_etudiant_inscription"><a href="inscription.php"><i class="fa fa-caret-right"></i> Inscription</a></li>
            <li><a href="form-validation.html"><i class="fa fa-caret-right"></i> Absences</a></li>
            <li><a href="form-validation.html"><i class="fa fa-caret-right"></i> Retards</a></li>
            <li><a href="form-wizards.html"><i class="fa fa-caret-right"></i> Bulletin</a></li>
            <li><a href="wysiwyg.html"><i class="fa fa-caret-right"></i> Documents</a></li>
            <li><a href="wysiwyg.html"><i class="fa fa-caret-right"></i> Emploi du temps</a></li>
          </ul>
        </li>
          <li><a href="index.html"><i class="fa fa-eur"></i> <span>Versement</span></a></li>
        <li class="nav-parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Modules</span></a>
          <ul class="children">
            <li><a href="buttons.html"><i class="fa fa-caret-right"></i> Modules</a></li>
            <li><a href="icons.html"><i class="fa fa-caret-right"></i> Matières</a></li>
            <li><a href="typography.html"><i class="fa fa-caret-right"></i> Controles</a></li>
          </ul>
        </li>
        <li class="nav-parent"><a href="#" id="menu_employer"><i class="fa fa-users"></i> <span>Employees</span></a>
          <ul class="children">
            <li id="menu_employer_professeurs"><a href="professeurs.php"><i class="fa fa-caret-right"></i> Professeurs</a></li>
            <li id="menu_employer_personnels"><a href="Personnels.php"><i class="fa fa-caret-right"></i> Personnels</a></li>
          </ul>
        </li>
          <li class="nav-parent"><a href="#" id="menu_parametrage"><i class="fa fa-cogs"></i> <span>Paramétrage</span></a>
              <ul class="children">
                  <li id="menu_parametrage_centres"><a href="centre.php"><i class="fa fa-caret-right"></i> Centres</a></li>
                  <li id="menu_parametrage_formation"><a href="formation.php"><i class="fa fa-caret-right"></i> Formations</a></li>
                  <li id="menu_parametrage_niveau"><a href="niveau.php"><i class="fa fa-caret-right"></i> Niveaux</a></li>
                  <li id="menu_parametrage_salle"><a href="salle.php"><i class="fa fa-caret-right"></i> Salles</a></li>
                  <li><a href="media-manager.html"><i class="fa fa-caret-right"></i> Profils</a></li>
                  <li id="menu_parametrage_utilisateur"><a href="utilisateur.php"><i class="fa fa-caret-right"></i> Utilisateurs</a></li>
                  <li id="menu_parametrage_journal"><a href="journal.php"><i class="fa fa-caret-right"></i> Journaux de logs</a></li>
              </ul>
          </li>
      </ul>
      
      <!--<div class="infosummary">
        <h5 class="sidebartitle">Information Summary</h5>    
        <ul>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Daily Traffic</span>
                    <h4>630, 201</h4>
                </div>
                <div id="sidebar-chart" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Average Users</span>
                    <h4>1, 332, 801</h4>
                </div>
                <div id="sidebar-chart2" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Disk Usage</span>
                    <h4>82.2%</h4>
                </div>
                <div id="sidebar-chart3" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">CPU Usage</span>
                    <h4>140.05 - 32</h4>
                </div>
                <div id="sidebar-chart4" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Memory Usage</span>
                    <h4>32.2%</h4>
                </div>
                <div id="sidebar-chart5" class="chart"></div>   
            </li>
        </ul>
      </div>-->  <!-- infosummary -->
      
    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->
  