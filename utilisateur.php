<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_parametrage";
$sousMenuActuel = "menu_parametrage_utilisateur";


$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script src="js/jquery.mousewheel.js"></script>
<script src="js/chosen.jquery.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });

  jQuery("a.delete-row").click(function(){
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
            swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
  });


  });
</script>

EOF;
?>
?>


<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Utilisateurs</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Paramétrage</a></li>
          <li class="active">Utilisateurs</li>
        </ol>
      </div>
    </div>
    
   





<div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Utilisateurs</h3>
            </div>
            <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>
                    <div class="table-responsive">
                       <table class="table table-hidaction table-bordered mb30">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Profil</th>
                                <th>login</th>
                                <th>Mot de passe</th>

                               
                         
                                  <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>user</td>
                                <td>Smoksi</td>
                                 <td>*********</td>
                                
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <tr>
                                  <td>2</td>
                                <td>root</td>
                                <td>John</td>
                                 <td>*********</td>
                               
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <tr>
                                  <td>3</td>
                                <td>user</td>
                                <td>leo</td>
                                 <td>*********</td>
                               
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>root</td>
                                <td>root</td>
                               <td>*********</td>
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>

                    <form class="form-horizontal form-bordered configForm" action="#.">

                        <div class="alert alert-success" id="alert-msg">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Well done!</strong> You successfully read this <a href="#" class="alert-link">important alert message</a>.
                        </div>
<div class="form-group">
                            <label class="col-sm-3 control-label">Profil <span class="asterisk">*</span></label>
                            <div class="col-sm-5">
                                <select class="form-control chosen-select" required data-placeholder="Choissiez un centre">
                                    <option value=""></option>
                                    <option value="United States">ROOT</option>
                                    <option value="United Kingdom">USER</option><br>
                                    
                                    </select>
                            </div>

                        <div class="form-group">
                           <br> <label class="col-sm-3 control-label">Login | Mot de passe <span class="asterisk">*</span></label>
                          
                            <div class="col-sm-6">
                                <input type="text" placeholder="Login" class="form-control" required />    
                                <input type="password" placeholder="Mot de passe" class="form-control" required />

                            </div>

                          
                        </div>
                        


                    




                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <button type="button" onclick="jQuery()" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </form>


                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

        

                   

                </div><!-- row -->
            </div>
        </div> <!-- /PANEL -->
    </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>
