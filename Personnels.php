<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_parametrage";
$sousMenuActuel = "menu_parametrage_centres";


$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script src="js/jquery.mousewheel.js"></script>
<script src="js/chosen.jquery.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });

  jQuery("a.delete-row").click(function(){
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
            swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
  });


  });
</script>

EOF;
?>

<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Employees <span>Personnels</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="centre.php">Personnels</a></li>
          <li class="active">Personnels</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Configuration des Personnels</h3>
            </div>
            <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5">Personnels</h5>
                    <div class="table-responsive">
                       <table class="table table-hidaction table-bordered mb30">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID_PROFILLE</th>
                                <th>LOGIN</th>
                                <th>PASSWORD</th>
                            </tr>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>5</td>
                                <td>FATIH</td>
                                <td>ESP1989</td>
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>

                            <tr>
                                <td>2</td>
                               <td>8</td>
                                <td>FRAGO</td>
                                <td>HCH89G7</td>
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>3</td>
                                <td>HARRATI</td>
                                <td>SOUHIL</td>
                                <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                                <td>4</td>
                                <td>8</td>
                                <td>BAHLAOUAN</td>
                                <td>HAMZA</td>
                                 <td class="table-action-hide">
                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tbody>
                            </thead>

                            
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>

                    <form class="form-horizontal form-bordered configForm" action="#.">
                    </form>
                 

 <div class="form-group">
                             <label class="col-sm-5 control-label">PROFILLE <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="col-sm-5">
                                <select class="form-control chosen-select" required data-placeholder="Choissiez une ville">
                                    <option value=""></option>
                                    <option value="United States"> ROOT</option>
                                    <option value="United States"> USER</option>
                                    <option value=""></option>

                                </select>
                            </div>
                                <input type="text" placeholder="PROFILLE" class="form-control" required />
                            </div>
                             <label class="col-sm-5 control-label">LOGIN <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="LOGIN" class="form-control" required />
                            </div>
                             <label class="col-sm-5 control-label">PASSWORD <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="PASSWORD" class="form-control" required />
                            </div>
                        </div>
                        <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <button type="button" onclick="jQuery()" class="btn btn-primary">Valider</button>
                                </div>
                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>