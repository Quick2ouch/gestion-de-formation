<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_etudiant";
$sousMenuActuel = "menu_etudiant_inscription";
$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script src="js/jquery.maskedinput.min.js"></script>

<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");



  });
 
   
    
     $( "#dp" ).datepicker({

  });
    // Input Masks
  //jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(99) 99-99-99-99");
  //jQuery("#ssn").mask("999-99-9999");
</script>

EOF;
?>


<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-home"></i> inscription <span>Subtitle goes here...</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Bracket</a></li>
          <li class="active">Blank</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
                <div class="col-xs- col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>

                    <form class="form-horizontal form-bordered configForm" action="#.">

                        <div class="alert alert-success" id="alert-msg">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Well done!</strong> You successf
ully read this <a href="#" class="alert-link">important alert message</a>.
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">Nom <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Nom" class="form-control" required />
                            </div>

                        </div>
                        <div class="form-group ">
                            <label class="col-sm-4 control-label">Prenom <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="prenom" class="form-control" required />
                            </div>
                            
                        </div>
                        <div class="form-group ">
                             <label class="col-sm-4 control-label" for="dp">Date Picker</label> 
                            <div class="col-sm-6">
                            <div class="input-group">
                 <input type="text" class="form-control" id="dp" placeholder="mm/dd/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>
                               
                            </div>
                            
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-4 control-label">Centre <span class="asterisk">*</span></label>
                            <div class="col-sm-5">
                                <select class="form-control chosen-select" required data-placeholder="Choissiez une ville">
                                    <option value=""></option>
                                    <option value="casa">casablanca</option>
                                    <option value="el jadida">el jadida</option>
                                    <option value="fes">fes</option>
                                    <option value="rabat">rabat</option>
                                    
         </select>
                            </div>
                        </div>
                         <div class="form-group ">
                            <label class="col-sm-4 control-label">Cin <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Cin" class="form-control" required />
                            </div>
                            
                        </div>
                         <div class="form-group ">
                            <label class="col-sm-4 control-label">Email <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="email" class="form-control" required />
                            </div>
                            
                        </div>
                         <div class="form-group ">
                            <label class="col-sm-4 control-label">Telephone <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                 <div class="input-group mb20">
                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                <input type="text" placeholder="Phone" id="phone" class="form-control" />
              </div>
              
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-13">
                                <button type="button" onclick="jQuery()" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </form>


                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>