<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_parametrage";
$sousMenuActuel = "menu_parametrage_journal";


$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF

<script>
jQuery(document).ready(function(){
    


  
  jQuery('.datepicker-multiplee').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });



  
});




</script>







<script>
jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  //jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  /*jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });*/

  jQuery("a.delete-row").click(function(){
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
            swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
  });

 
  });

</script>



EOF;
?>
?>



<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Journal de log</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Paramétrage</a></li>
          <li class="active">Journal de log</li>
        </ol>
      </div>
    </div>
    
   





<div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Journal de log</h3>
            </div>
            <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>
                    <div class="table-responsive">
                       
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

 <div class="contentpanel">


     <div class="row">
         <div class="col-md-6">
             <label>Du:</label>
             <div class="input-group">
                 <input type="text" class="form-control datepicker-multiplee" placeholder="mm/dd/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>


         </div>
         <div class="col-md-6">

             <label>À:</label>
             <div class="input-group">
                 <input type="text" class="form-control datepicker-multiplee" placeholder="mm/dd/yyyy">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>
         </div>
     </div>


              <p class="text-center">
                <br><button class="btn btn-primary btn-lg">Telecharger</button>
              </p>
               </div>
           
            </div><!-- panel-body -->
          </div><!-- panel -->
          
        </div>

                        </div>
                        
                        </div>
                    </form>


                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

        

                   

                </div><!-- row -->
            </div>
        </div> <!-- /PANEL -->
    </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>
